﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using mvcHaber.Models;
using PagedList;
using PagedList.Mvc;

namespace mvcHaber.Controllers
{
    public class HomeController : Controller
    {
        mvchaberDB db = new mvchaberDB();
        // GET: Home
        public ActionResult Index(int Page=1)
        {
            var makale = db.Makales.OrderByDescending(m => m.MakaleId).ToPagedList(Page,5);
            
            return View(makale);
           
        }
        public ActionResult HaberAra(string Ara = null)
        {
            var aranan = db.Makales.Where(m => m.Baslik.Contains(Ara)).ToList();
            return View(aranan.OrderByDescending(m => m.Tarih));
        }
        public ActionResult PopulerHaberler()
        {
            return View(db.Makales.OrderByDescending(m => m.Okunma).Take(5));
        }
        public ActionResult SonHaberler()
        {
            return View(db.Makales.OrderByDescending(m => m.MakaleId).Take(5));
        }

        public ActionResult MakaleDetay(int id)
        {
            var makale = db.Makales.Where(m => m.MakaleId == id).SingleOrDefault();
            makale.Okunma = makale.Okunma + 1;
            db.SaveChanges();
            if (makale == null)
            {
                return HttpNotFound();

            }
            return View(makale);
        }
        public ActionResult KategoriMakale(int id)
        {
            var makaleler = db.Makales.Where(m => m.Kategori.KategoriId == id).ToList();
            return View(makaleler);
        }

        public ActionResult Hakkimizda()
        {
            return View();
        }
        public ActionResult Iletisim()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Iletisim(string adSoyad, string email, string konu,  string mesaj)
        {
            if (adSoyad != null)
            {
                WebMail.SmtpServer = "smtp.gmail.com";
                WebMail.EnableSsl = true;
                WebMail.UserName = "mail adresi";
                WebMail.Password = "sifre";
                WebMail.SmtpPort = 587;
                WebMail.Send("trakyacopy@gmail.com" , konu , mesaj);
                ViewBag.Uyari = "Mesaj Başarıyla Gönderildi";
            }
            else
            {
                ViewBag.Uyari = "Hata Oluştu";
            }
            return View();
        }

        public ActionResult KategoriPartial()
        {
            return View(db.Kategoris.ToList());
        }

        public ActionResult SliderPartial()
        {
            return View(db.Sliders.ToList());
        }
    }
}